const express= require('express');
const mongoose= require('mongoose');
const stuffRoute= require('./routes/stuff');
const userRoutes= require('./routes/user');
const app= express();
const path= require('path');


mongoose.connect('mongodb+srv://user_test:test@cluster0.17w5j.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

const bodyParser= require('body-parser');

app.use((req, res, next) => {
    console.log('Requête reçue !');
    next(); 
});
app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());

app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/api/auth', userRoutes);

app.use('/api/stuff', stuffRoute);

module.exports= app;