const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try{

      const options= req.method;
    
      if(options === 'GET'){

      const token = req.headers.authorization.split(' ')[1];
      const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET_LONG');
      const userId = decodedToken.userId;
      if (req.body.userId && req.body.userId !== userId) {
        throw 'Invalid user ID';
      } else {
        next();
      }
      }
      else{
        next();
      }
  } catch(error) {
    res.status(401).json({error: error | 'Invalid request!'});
  }
};