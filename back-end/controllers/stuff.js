const Thing= require('../models/things');
const fs= require('fs')

exports.createThing= (req,res,next)=>{
  const objectThing= JSON.parse(req.body.thing);
  console.log(objectThing);
  delete objectThing._id;
   /* delete req.body._id;*/
    const thing =new Thing({
     /* ...req.body // AU lien de faire: title: req.body.title ainsi de suite.*/
     ...objectThing,
     imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });
    thing.save()
    .then(()=>{res.status(201).json({message: "Données bien enrégistrées"})})
    .catch((error)=> {res.status(400).json({error})});
    }

exports.getOneThing= (req,res,next)=>{
    Thing.findOne({ _id: req.params.id})
    .then((thing)=> {res.status(200).json(thing)})
    .catch((error)=>{res.status(404).json({error})});
    
  }

exports.modifyThing= (req, res,next)=>{
  const objectThing= req.file ? 
  { 
    ...JSON.parse(req.body.thing),
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  }:
  {...req.body};

    Thing.updateOne({_id: req.params.id}, {...objectThing, _id: req.params.id})
    .then(()=>{res.status(200).json({message: "Objet bien modifié"})})
    .catch((error)=>{res.status(400).json({error})}); 
  }

exports.deleteThing= (req,res, next)=>{
  Thing.findOne({_id: req.params.id})
  .then((thing) =>{
    const fileName= thing.imageUrl.split('/images/')[1];
    fs.unlink(`images/${fileName}` , ()=>{
      Thing.deleteOne({_id: req.params.id})
      .then(()=>{res.status(200).json({message:"Objet bien supprimé"})})
      .catch((error)=>{res.status(400).json({error})}); 
    });
  })
  .catch((error)=>{res.status(400).json({error})}); 


  }

exports.getAllThing= (req,res,next)=>{
    Thing.find()
    .then((things)=> { res.status(200).json(things)})
    .catch((error)=>{res.status(400).json({error})}); 
      next();
}